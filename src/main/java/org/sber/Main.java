package org.sber;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        FixedThreadPool fixedThreadPool = new FixedThreadPool(2);
        fixedThreadPool.start();
        for (int i = 0; i < 3; i++) {
            fixedThreadPool.execute(()-> {
                for (int j = 0; j < 5; j++) {
                    System.out.print(j + " ");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        Thread.sleep(10000);


        ScalableThreadPool scalableThreadPool = new ScalableThreadPool(2, 5);
        scalableThreadPool.start();
        for (int i = 0; i < 10; i++) {
            scalableThreadPool.execute(() -> {
                for (int j = 0; j < 5; j++) {
                    System.out.print(j + " " + Thread.currentThread().getName());
                    System.out.println();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }



    }
}
