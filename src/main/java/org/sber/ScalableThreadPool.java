package org.sber;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class ScalableThreadPool implements ThreadPool {

    private  final int max;
    private final int min;
    private final BlockingQueue<Runnable> queue;
    private  List<Thread> threads;
 //   private AtomicInteger atomic ;


    public ScalableThreadPool(int min, int max) {
        this.min = min;
        this.max = max;
        queue = new LinkedBlockingQueue<>();
        threads = new ArrayList<>();
      //  atomic = new AtomicInteger(0);
    }

    @Override
    public void start() {
        for (int i = 0; i < min; i++) {
            threads.add(new MyThread());
            threads.get(i).start();
        }
    }

    @Override
    public void execute(Runnable runnable) {
        if (isWaitThreads()){
            for (int i = min; i < max; i++) {
                threads.add( new MyThread());
                threads.get(i).start();
            }
            System.out.println("ThreadPool увелиен до " + threads.size());
        }
        if (queue.isEmpty() && threads.size() == max) {
            List<Thread> listForStopThread = threads.subList(min, threads.size() - 1);
            listForStopThread.forEach(Thread::interrupt);
            threads = threads.subList(0, min);
        }
        synchronized (queue) {
            try {
                queue.put(runnable);
             //   atomic.incrementAndGet();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            queue.notify();
        }
    }

    private boolean isWaitThreads(){
        for (Thread t : threads){
            if (t.getState().equals(Thread.State.RUNNABLE)){
                return true;
            }
        }
        return false;
    }

    private class MyThread extends Thread {
        @Override
        public void run() {
            Runnable task;
            while (!Thread.currentThread().isInterrupted()) {
                synchronized (queue) {
                    if (queue.isEmpty() ){
                        try {
                            queue.wait();
                        } catch (InterruptedException e) {
                            System.out.println(Thread.currentThread().getName() + " Поток прерван из-за уменьшения размера пула потоков");
                        }
                    }
                    task = queue.poll();
                }
                task.run();
              //  atomic.decrementAndGet();
            }
        }
    }
}

