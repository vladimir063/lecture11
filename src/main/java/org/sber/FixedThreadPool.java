package org.sber;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class FixedThreadPool implements ThreadPool{

    private final int countThread;
    private final BlockingQueue<Runnable> queue;
    private final Thread[] threads;


    public FixedThreadPool(int countThread) {
        this.countThread = countThread;
        queue = new LinkedBlockingQueue<>();
        threads = new Thread[countThread];
    }

    @Override
    public void start() {
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new MyThread();
            threads[i].start();
        }
    }


    @Override
    public void execute(Runnable runnable) {
        synchronized (queue) {
            try {
                queue.put(runnable);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            queue.notify();
        }
    }

    private class MyThread extends Thread {

        @Override
        public void run() {
            Runnable task;
            while (true){
                synchronized (queue) {
                    if (queue.isEmpty() ){
                        try {
                            queue.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    task = queue.poll();
                }
                task.run();

            }
        }
    }
}



